#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>

#define N 64
#define M 256
#define max_targets 512
#define NUM_SIDE 6

typedef signed char int8_T;
typedef unsigned char uint8_T;
typedef short int16_T;
typedef unsigned short uint16_T;
typedef int int32_T;
typedef long long long_int32_T;
typedef unsigned int uint32_T;
typedef float real32_T;
typedef double real64_T;

#define _CRT_SECURE_NO_WARNIGNS
#pragma warning(disable : 4996)

using namespace std;

typedef struct {
    int rng_idx[max_targets];
    int dop_idx[max_targets];
    int num_targs;
}targ_params;

void findPrefixSums2D(double a[][M], long double b[][M + 1], int n, int m);
void findPrefixSums2D(double* a, long double* b, int n, int m);
void findPrefixSums2D(int32_T* a, long_int32_T* b, int n, int m);
targ_params prefixCFAR(int32_T* input_array, int num_train, int num_guard);
targ_params prefixCFAR(double* input_array, int num_train, int num_guard);
targ_params prefixCFAR(double input_array[][M], int num_train, int num_guard);
