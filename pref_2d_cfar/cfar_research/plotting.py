# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 15:44:56 2021

@author: Belyaev
"""

import numpy as np
import matplotlib.pyplot as plt
two_dims_CFAR = np.loadtxt('output.txt')

# plt.figure()
# plt.imshow(two_dims_CFAR,
#            cmap=plt.cm.get_cmap('hot'))
# plt.title('RV_map')
# plt.xlabel('Отсчёты по скорости')
# plt.ylabel('Отсчёты по дальности')
# plt.show()

plt.figure()
plt.imshow(np.fft.fftshift(two_dims_CFAR, 0),
            cmap=plt.cm.get_cmap('hot'))
plt.title('RV_map')
plt.xlabel('Отсчёты по скорости')
plt.ylabel('Отсчёты по дальности')
plt.show()
