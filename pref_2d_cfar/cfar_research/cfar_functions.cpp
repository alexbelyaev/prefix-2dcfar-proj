﻿#include "cfar_functions.h"


void findPrefixSums2D(double a[][M], long double b[][M + 1], int n, int m) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            double bi1j = b[i + 1][j];
            double bij1 = b[i][j + 1];
            double bij = b[i][j];
            double aij = a[i][j];
            b[i + 1][j + 1] = b[i + 1][j] + b[i][j + 1] - b[i][j] + a[i][j];
        }
    }
}

/*
перегрузка функции findPrefixSums2D для случая, когда входной двумерный массив представлен ввиде одномерного
*/

void findPrefixSums2D(double* a, long double* b, int n, int m) {

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            double bi1j = b[(i + 1) * (m + 1) + j];
            double bij1 = b[i * (m + 1) + j + 1];
            double bij = b[i * (m + 1) + j];
            double aij = a[i * m + j];
            b[(i + 1) * (m + 1) + j + 1] = bi1j + bij1 - bij + aij;
        }
    }

}

/*
перегрузка функции findPrefixSums2D для целочисленного случая
*/

void findPrefixSums2D(int32_T* a, long_int32_T* b, int n, int m) {

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            int bi1j = b[(i + 1) * (m + 1) + j];
            int bij1 = b[i * (m + 1) + j + 1];
            int bij = b[i * (m + 1) + j];
            int aij = a[i * m + j];
            b[(i + 1) * (m + 1) + j + 1] = bi1j + bij1 - bij + aij;
        }
    }

}

targ_params prefixCFAR(double input_array[][M], int num_train, int num_guard) {

    targ_params params;
    for (int i = 0; i < max_targets; i++) {
        params.dop_idx[i] = 0;
        params.rng_idx[i] = 0;
    }

    long double b[N + 1][M + 1] = { 0 };
    findPrefixSums2D(input_array, b, N, M);

    int cfar_offset = 0;
    double fa_rate = 1e-1;
    double trainfactor = num_train * num_train;
    double alpha = trainfactor * (pow(fa_rate, -1 / trainfactor) - 1);

    int full_win_side = 2 * (num_train + num_guard) + 1;
    int guard_win_side = 2 * (num_guard + 1);
    int analize_square = full_win_side * full_win_side - guard_win_side * guard_win_side;

    double big_sum = 0;
    double little_sum = 0;
    double apper_sum = 0;
    int lx, ly, rx, ry;
    int num_side = num_train + num_guard;

    for (int n = num_side; n < N - num_side; n++) {
        for (int m = num_side; m < M - num_side; m++) {
            lx = n - num_side;
            ly = m - num_side;
            rx = n + num_side;
            ry = m + num_side;
            big_sum = b[rx][ry] - b[lx][ry] - b[rx][ly] + b[lx][ly];

            lx = n - num_guard;
            ly = m - num_guard;
            rx = n + num_guard;
            ry = m + num_guard;
            little_sum = b[rx][ry] - b[lx][ry] - b[rx][ly] + b[lx][ly];

            apper_sum = big_sum - little_sum;

            double cfar_treshold = cfar_offset + alpha * (apper_sum / trainfactor);
            if (input_array[n][m] > cfar_treshold) {
                if ((input_array[n][m] > input_array[n - 1][m]) && (input_array[n][m] > input_array[n + 1][m])
                    && (input_array[n][m] > input_array[n][m + 1]) && (input_array[n][m] > input_array[n][m - 1])) {
                    params.rng_idx[params.num_targs] = m;
                    params.dop_idx[params.num_targs] = n;
                    params.num_targs++;
                }
            }
        }
    }
    return params;
}

/*
перегрузка функции prefixCFAR для целочисленного случая
*/

targ_params prefixCFAR(int32_T* input_array, int num_train, int num_guard) {

    int rng_idx[max_targets] = { 0 };
    int dop_idx[max_targets] = { 0 };

    targ_params params;
    for (int i = 0; i < max_targets; i++) {
        params.dop_idx[i] = 0;
        params.rng_idx[i] = 0;
    }
    params.num_targs = 0;

    params.num_targs = 0;
    int num_side = num_train + num_guard;

    int32_T a[(N + NUM_SIDE * 2) * M] = { 0 };

    int j = 0;
    for (int i = 0; i < NUM_SIDE * M; i++) {
        a[i] = input_array[j];
        j++;
    }
    j = 0;
    for (int i = NUM_SIDE * M; i < (N + NUM_SIDE) * M; i++) {
        a[i] = input_array[j];
        j++;
    }
    j = 0;
    for (int i = (N + NUM_SIDE) * M; i < (N + NUM_SIDE * 2) * M; i++) {
        a[i] = input_array[j + (N - NUM_SIDE) * M];
    }
    j = 0;

    long_int32_T b[(N + NUM_SIDE * 2 + 1) * (M + 1)] = { 0 };
    findPrefixSums2D(a, b, N+1, M+1);

    int cfar_offset = 0;
    double fa_rate = 1e-1;
    double trainfactor = num_train * num_train;
    double alpha = trainfactor * (pow(fa_rate, -1 / trainfactor) - 1);

    long_int32_T big_sum = 0;
    long_int32_T little_sum = 0;
    long_int32_T apper_sum = 0;
    int lx, ly, rx, ry;
    int num_side_ = num_train + num_guard;

    for (int n = num_side; n < N - num_side; n++) {
        for (int m = num_side; m < M - num_side; m++) {
            lx = n - num_side;
            ly = m - num_side;
            rx = n + num_side;
            ry = m + num_side;
            big_sum = b[rx * (M + 1) + ry] - b[lx * (M + 1) + ry] - b[rx * (M + 1) + ly] + b[lx * (M + 1) + ly];

            lx = n - num_guard;
            ly = m - num_guard;
            rx = n + num_guard;
            ry = m + num_guard;
            little_sum = b[rx * (M + 1) + ry] - b[lx * (M + 1) + ry] - b[rx * (M + 1) + ly] + b[lx * (M + 1) + ly];

            apper_sum = big_sum - little_sum;

            double cfar_treshold = cfar_offset + alpha * (apper_sum / trainfactor);
            if (input_array[n * M + m] > cfar_treshold) {
                if ((input_array[n * M + m] > input_array[(n - 1) * M + m]) && (input_array[n * M + m] > input_array[(n + 1) * M + m])
                    && (input_array[n * M + m] > input_array[n * M + m + 1]) && (input_array[n * M + m] > input_array[n * M + m - 1])) {
                    params.rng_idx[params.num_targs] = m;
                    params.dop_idx[params.num_targs] = n;
                    params.num_targs++;
                }

            }
        }
    }

    return params;
}


/*
перегрузка функции prefixCFAR для случая, когда входной двумерный массив представлен ввиде одномерного
*/

targ_params prefixCFAR(double* input_array, int num_train, int num_guard) {

    int rng_idx[max_targets] = { 0 };
    int dop_idx[max_targets] = { 0 };

    targ_params params;
    for (int i = 0; i < max_targets; i++) {
        params.dop_idx[i] = 0;
        params.rng_idx[i] = 0;
    }
    params.num_targs = 0;
    int num_side = num_train + num_guard;

    double a[(N + NUM_SIDE*2) * M] = { 0 };
    long double b[(N + NUM_SIDE *2 + 1) * (M + 1)] = { 0 };

    int j = 0;
    for (int i = 0; i < num_side * M; i++) {
        a[i] = input_array[j];
        j++;
    }
    j = 0;
    for (int i = num_side * M; i < (N + num_side) * M; i++) {
        a[i] = input_array[j];
        j++;
    }
    j = 0;
    for (int i = (N + num_side) * M; i < (N + num_side * 2) * M; i++) {
        a[i] = input_array[j + (N - num_side)*M ];
    }
    j = 0;

    findPrefixSums2D(a, b, N + 2 * num_side, M);

    int cfar_offset = 0;
    double fa_rate = 1e-1;
    double trainfactor = num_train * num_train;
    double alpha = trainfactor * (pow(fa_rate, -1 / trainfactor) - 1);

    double big_sum = 0;
    double little_sum = 0;
    double apper_sum = 0;
    int lx, ly, rx, ry; 

    // main inner part
    for (int n = num_side; n < N; n++) {
        for (int m = num_side; m < M; m++) {
            lx = n - num_side;
            ly = m - num_side;
            rx = n + num_side + 1;
            ry = m + num_side + 1;
            double rxry = b[rx * (M + 1) + ry];
            double lxry = b[lx * (M + 1) + ry];
            double rxly = b[rx * (M + 1) + ly];
            double lxly = b[lx * (M + 1) + ly];
            big_sum = rxry - lxry - rxly + lxly;

            lx = n - num_guard;
            ly = m - num_guard;
            rx = n + num_guard + 1;
            ry = m + num_guard + 1;
            little_sum = b[rx * (M + 1) + ry] - b[lx * (M + 1) + ry] - b[rx * (M + 1) + ly] + b[lx * (M + 1) + ly];

            apper_sum = big_sum - little_sum;

            double cfar_treshold = cfar_offset + alpha * (apper_sum / trainfactor);
            if (a[n * M + m] > cfar_treshold) {
                if ((a[n * M + m] > a[(n - 1) * M + m]) && (a[n * M + m] > a[(n + 1) * M + m])
                    && (a[n * M + m] > a[n * M + m + 1]) && (a[n * M + m] > a[n * M + m - 1])) {
                    if (m < M - num_side) {
                        params.rng_idx[params.num_targs] = m;
                        params.dop_idx[params.num_targs] = n - num_side;
                        params.num_targs++;
                    }
                }
            }
        }
    }

    return params;
}
