﻿#include "cfar_functions.h"

using namespace std;

int main(){

    FILE* mas_CFAR = fopen("input_frame.txt", "r");

    double number_array[N * M] = {0};
    int number_array_int[N * M] = { 0 };

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            fscanf(mas_CFAR, "%le", &number_array[i*M + j]);
            fscanf(mas_CFAR, "%d", &number_array_int[i * M + j]);
        }
    }

    for (int i = 0; i < N; i++) {
        cout << number_array[i*M + 7] << ' ' << i << ' ' << number_array[i * M + N - 1] << ' ' << i << '\n';
        for (int j = 0; j < M; j++) {
            
        }
        cout << std::endl;
    }

    double num_train = 4;
    int num_guard = 2;
    int N_ = 2*(num_train + num_guard) + 1;
    int M_ = N_;

    /*vector<vector<int>> a(N, vector<int>(M, 0));

    for (int i = 0; i < N_; i++) 
        for (int j = 0; j < M_; j++)
            a[i][j] = i * M_ + j + 1;
    a[num_train + num_guard][num_train + num_guard] = 0;

    const clock_t begin_time = clock();

    for (int n = 0; n < big_mas_N; n++) {
        for (int m = 0; m < big_mas_M; m++) {

            int sum = 0;
            for (int i = 0; i < N_; i++) {
                for (int j = 0; j < M_; j++) {
                    if ((i >= num_train) && (i <= num_train + num_guard)) {
                        if ((j >= num_train) && (j <= num_train + num_guard)) {
                            break;
                        }
                        sum += number_array[i][j];
                    }
                    else {
                        sum += number_array[i][j];
                    }
                }
            }
        
        }
    }

    const clock_t end_time = clock();
    std::cout<< "\nmsecs need for classic CFAR = " << float(end_time - begin_time) / 1 << '\n';
    */

    double square_number_array[N*M];
    int square_number_array_int[N * M];
    long double output_array[N*M] = {};
    targ_params targets_params;
    targ_params targets_params_int;

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            square_number_array[i*M + j] = number_array[i*M + j] * number_array[i * M + j];
            square_number_array_int[i * M + j] = number_array_int[i * M + j] * number_array_int[i * M + j];
        }
    }

    const clock_t begin_time_pref = clock();

    for (int i = 0; i < 100; i++) {
//        targets_params = prefixCFAR(square_number_array, 4, 2);
        targets_params_int = prefixCFAR(square_number_array_int, 4, 2);
    }

 //   targets_params_int = prefixCFAR(square_number_array_int, 4, 2);

    const clock_t end_time_pref = clock();
    std::cout << "msecs need for preffix CFAR = " << float(end_time_pref - begin_time_pref) / 1 << '\n';
    
    targets_params = prefixCFAR(square_number_array, 4, 2);
    for (int i = 0; i < targets_params.num_targs; i++) {
        int n = targets_params.dop_idx[i];
        int m = targets_params.rng_idx[i];
        output_array[n*M + m] = 1;
    }

    ofstream outputfile;
    outputfile.open("output.txt");

    for (int n = 0; n < N; n++) {
        for (int m = 0; m < M; m++) {
            outputfile << output_array[n*M + m] << ' ';//saves values to file
        }
        outputfile << std::endl;
    }

    outputfile.close();

}
